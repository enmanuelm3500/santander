Proyecto desarrollado con angular 9 y java 8 el paso a paso para la implementacion esta en documento dentro de la carpeta del mismo nombre, en cada proyecto,
los usuarios de prueba son:

user: admin, pass: 12345
user: user_1, pass: 12345
user: user_2, pass: 12345

para la persistencia se utilizo h2, una base de datos en memoria.

para probar el servicio de notificaciones de meetUps via mail, se recomienda actualizar un registro de usuario con un mail, donde puedan verificar la resepcion de
la notificacion, esta actualizacion debe hacerce en el archivo import.sql ubicado en: \back\src\main\resources.