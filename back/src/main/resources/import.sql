INSERT INTO `users` (username, password, enabled, mail) VALUES ('user_1','$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG',1, 'deogracio350@gmail.com');
INSERT INTO `users` (username, password, enabled, mail) VALUES ('admin','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1, 'enmanuelm3500@gmail.com');
INSERT INTO `users` (username, password, enabled, mail) VALUES ('user_2','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1, 'enmanuelm3500@gmail.com');

INSERT INTO `authorities` (user_id, authority) VALUES (1,'ROLE_USER');
INSERT INTO `authorities` (user_id, authority) VALUES (2,'ROLE_ADMIN');
INSERT INTO `authorities` (user_id, authority) VALUES (2,'ROLE_USER');
INSERT INTO `authorities` (user_id, authority) VALUES (3,'ROLE_USER');


INSERT INTO `status` (status_id, name) VALUES (1,'INVITADO');
INSERT INTO `status` (status_id, name) VALUES (2,'INSCRITO');
INSERT INTO `status` (status_id, name) VALUES (3,'ASISTIDO');

INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (1, 1, 1, 1);
INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (2, 2, 1, 2);

INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (3, 1, 2, 1);
INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (4, 2, 2, 2);

INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (5, 1, 3, 1);
INSERT INTO `meetuser` (meetuser_id, status_id, usuario_id, meet_id) VALUES (6, 2, 3, 2);



INSERT INTO `city` (city_id, name, lng, lat) VALUES (1,'Buenos aires', '-34.616', '-58.3469');
INSERT INTO `city` (city_id, name, lng, lat) VALUES (2,'Bariloche', '-34.616', '-58.3469');
INSERT INTO `city` (city_id, name, lng, lat) VALUES (3,'Tucuman', '-34.616', '-58.3469');
INSERT INTO `city` (city_id, name, lng, lat) VALUES (4,'Mendoza', '-34.616', '-58.3469');


INSERT INTO `meet` (id, street, housenumber, postalcode, city_id, phone, date, email, topic) VALUES (1,'SAN LUIS', '2345', '1872',3, '1125705064', '2020-03-11T23:04', 'co@co.com','tema');
INSERT INTO `meet` (id, street, housenumber, postalcode, city_id, phone, date, email, topic) VALUES (2,'SAN LUIS', '2345', '1872',1, '1125705064', '2020-05-12T23:04', 'tema2@co.com','tema2');









