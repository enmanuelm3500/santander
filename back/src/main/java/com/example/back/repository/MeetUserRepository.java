package com.example.back.repository;

import com.example.back.entity.MeetUser;
import com.example.back.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetUserRepository extends JpaRepository<MeetUser, Integer> {

    MeetUser findByMeetAndUsuario(Integer idMeet, Integer idUser);

    Integer countByMeetAndStatus(Integer meet, Status status);

}
