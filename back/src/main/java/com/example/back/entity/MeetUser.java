package com.example.back.entity;


import javax.persistence.*;

@Entity
@Table(name = "meetuser")
public class MeetUser {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meetuser_id")
    private Integer id;

    @OneToOne(targetEntity = Status.class)
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "usuario_id")
    private Integer usuario;

    @Column(name = "meet_id")
    private Integer meet;

    public MeetUser(Status status, Integer usuario, Integer meet) {
        this.status = status;
        this.usuario = usuario;
        this.meet = meet;
    }

    public MeetUser() {
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getMeet() {
        return meet;
    }

    public void setMeet(Integer meet) {
        this.meet = meet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
