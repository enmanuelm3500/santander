package com.example.back.service.impl;

import com.example.back.entity.Users;
import com.example.back.repository.UsersRepository;
import com.example.back.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<Users> getList() {
        return usersRepository.findAll();
    }

    @Override
    public Users findByName(String name) {
        return usersRepository.findByUsername(name);
    }
}
