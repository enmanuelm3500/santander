package com.example.back.service;

import com.example.back.dto.BasicDTO;
import com.example.back.dto.MeetDTO;
import com.example.back.dto.MeetUserDTO;
import com.example.back.entity.Meet;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface MeetService {

    List<Meet> getList();

    Meet createMeet(Meet meet);

    MeetDTO setMeetUser(MeetDTO dto, Authentication authentication);

    MeetUserDTO changeStatus(Integer idMeetUser);

    BasicDTO countAsists(Integer idMeet);

}
