package com.example.back.service;

import com.example.back.entity.MeetUser;
import com.example.back.entity.Status;
import org.springframework.security.core.Authentication;

public interface MeetUserService {

    void create(Status status, Integer usuario, Integer meet);

    MeetUser findByMeetAndUser(Integer idMeet, Integer user);

    MeetUser changeStatus(Integer idMeetUser);

    MeetUser findById(Integer idMeetUser);

    Integer countAsists(Integer idMeetUser);


}
