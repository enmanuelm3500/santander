package com.example.back.service;

public interface MailService {

    void sendEmail(String to, String subject, String content);

}
