package com.example.back.service;

import com.example.back.entity.City;

import java.util.List;

public interface CityService {

    List<City> getList();

    City getById(Integer idCity);

}
