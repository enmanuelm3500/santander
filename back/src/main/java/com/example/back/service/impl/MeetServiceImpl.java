package com.example.back.service.impl;

import com.example.back.dto.BasicDTO;
import com.example.back.dto.MeetDTO;
import com.example.back.dto.MeetUserDTO;
import com.example.back.entity.Meet;
import com.example.back.entity.MeetUser;
import com.example.back.entity.Status;
import com.example.back.entity.Users;
import com.example.back.repository.MeetRepository;
import com.example.back.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeetServiceImpl implements MeetService {

    private final static String SUBJECT_MAIL = " Invitacion a Nueva MeetUp ";

    private final static String CONTENT_MAIL = " Has sido invitado a una nueva meetUp, el tema a tratar: " +
            "%s, la misma se llevara a cabo en la ciudad de %s, el dia %s, para mas informacion puede escribir al correo electronico: %s";

    @Autowired
    private MeetRepository meetRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private UsersService usersService;

    @Autowired
    private MeetUserService meetUserService;

    @Autowired
    private StatusService statusService;

    @Value("${url.atm.api}")
    private String urlApi;

    @Override
    public Meet createMeet(Meet meet) {
        List<Users> users = usersService.getList();
        Meet meetSaved = meetRepository.save(meet);
        users.forEach(users1 -> sendMail(users1, meetSaved));
        return meetSaved;
    }

    @Override
    public MeetDTO setMeetUser(MeetDTO dto, Authentication authentication) {
        String user = String.valueOf(authentication.getPrincipal());
        return convertToMeetUserDTO(dto,meetUserService.findByMeetAndUser(dto.getId(), usersService.findByName(user).getId().intValue()));
    }

    @Override
    public MeetUserDTO changeStatus(Integer idMeetUser) {
        return newMeetUserDTO(meetUserService.changeStatus(idMeetUser));
    }

    @Override
    public BasicDTO countAsists(Integer idMeet) {
        BasicDTO response = new BasicDTO();
        response.setMessage(meetUserService.countAsists(idMeet));
        return response;
    }

    @Override
    public List<Meet> getList() {
        return meetRepository.findAll();
    }

    private void createMeetUser(Status status, Integer usuario, Integer meet) {
        meetUserService.create(status, usuario, meet);
    }

    private void sendMail(Users users, Meet meet) {
        createMeetUser(statusService.findById(1), users.getId().intValue(), meet.getId());
        mailService.sendEmail(users.getMail(), SUBJECT_MAIL, String.format(CONTENT_MAIL, meet.getTopic(), meet.getCity().getName(), meet.getDate(), meet.getEmail()));
    }

    private MeetDTO convertToMeetUserDTO(MeetDTO dto, MeetUser meetUser) {
        dto.setStatus(meetUser.getStatus().getId());
        dto.setIdMeetUser(meetUser.getId());
        return dto;
    }

    private MeetUserDTO newMeetUserDTO(MeetUser meetUser){
        MeetUserDTO dto = new MeetUserDTO();
        dto.setId(meetUser.getId());
        dto.setStatus(meetUser.getStatus().getId());
        return dto;
    }


}
