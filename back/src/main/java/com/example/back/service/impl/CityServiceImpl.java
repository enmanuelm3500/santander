package com.example.back.service.impl;

import com.example.back.entity.City;
import com.example.back.repository.CityRepository;
import com.example.back.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@Service
public class CityServiceImpl implements CityService {

    private static final String ENTITY_NOT_FOUND = "No existe una ciudad con el Id proporcionado";

    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<City> getList() {
        return cityRepository.findAll();
    }

    @Override
    public City getById(Integer idCity) {
        return cityRepository.findById(idCity).orElseThrow(() -> new EntityNotFoundException(ENTITY_NOT_FOUND));
    }
}
