package com.example.back.service;

import com.example.back.entity.Users;

import java.util.List;

public interface UsersService {

    List<Users> getList();

    Users findByName(String name);

}
