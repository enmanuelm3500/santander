package com.example.back.service.impl;

import com.example.back.entity.Status;
import com.example.back.repository.StatusRepository;
import com.example.back.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class StatusServiceImpl implements StatusService {

    private static final String ENTITY_NOT_FOUND = "No existe un Estado con el Id proporcionado";


    @Autowired
    private StatusRepository statusRepository;

    @Override
    public Status findById(Integer id) {
        return statusRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(ENTITY_NOT_FOUND));
    }
}
