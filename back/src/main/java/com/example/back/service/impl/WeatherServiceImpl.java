package com.example.back.service.impl;

import com.example.back.dto.ApiWeatherDTO;
import com.example.back.dto.SingleWatherDTO;
import com.example.back.entity.City;
import com.example.back.service.CityService;
import com.example.back.service.WeatherService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;


@Service
public class WeatherServiceImpl implements WeatherService {

    private static final double MATCH_TO_CENT = 273.15;


    @Autowired
    private CityService cityService;

    @Value("${url.atm.api}")
    private String urlApi;

    @Override
    public SingleWatherDTO reviewWeather(Integer idCity) {
        City city = consultingCityById(idCity);
        ApiWeatherDTO apiResponse = new Gson().fromJson(consultingAPi(city).getBody(), ApiWeatherDTO.class);
        return generateSingleResponse(apiResponse.getMain().getTemp());
    }

    private ResponseEntity<String> consultingAPi(City city) {
        RestTemplate template = new RestTemplate();
        return template.exchange(urlApi.concat(city.getName()), HttpMethod.GET, generateHeader(), String.class);
    }

    private HttpEntity generateHeader() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("x-rapidapi-key", "d3957f1772msh2d5aec3e575b564p193a70jsne658c5b174a3");
        requestHeaders.set("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com");
        HttpEntity entity = new HttpEntity<>(requestHeaders);
        return entity;
    }

    private City consultingCityById(Integer idCity) {
        return cityService.getById(idCity);
    }

    private SingleWatherDTO generateSingleResponse(double grados){
        return new SingleWatherDTO(convertToCentigrados(grados));
    }

    private double convertToCentigrados(double grados){
        return (grados - MATCH_TO_CENT);
    }


}
