package com.example.back.service;

import com.example.back.dto.SingleWatherDTO;

public interface WeatherService {

    SingleWatherDTO reviewWeather(Integer idCity);

}
