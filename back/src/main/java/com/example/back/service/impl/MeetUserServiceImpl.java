package com.example.back.service.impl;


import com.example.back.entity.MeetUser;
import com.example.back.entity.Status;
import com.example.back.repository.MeetUserRepository;
import com.example.back.repository.StatusRepository;
import com.example.back.service.MeetUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class MeetUserServiceImpl implements MeetUserService {

    private static final String ENTITY_NOT_FOUND = "No existe una meet con el Id proporcionado";


    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private MeetUserRepository meetUserRepository;

    @Override
    public void create(Status status, Integer usuario, Integer meet) {
        meetUserRepository.save(new MeetUser(status, usuario, meet));
    }

    @Override
    public MeetUser findByMeetAndUser(Integer idMeet, Integer idUser) {
        return meetUserRepository.findByMeetAndUsuario(idMeet, idUser);
    }

    @Override
    public MeetUser changeStatus(Integer idMeetUser) {
        MeetUser meetUser = findById(idMeetUser);
        meetUser.setStatus(statusRepository.getOne(meetUser.getStatus().getId() + 1));
        return meetUserRepository.save(meetUser);
    }

    @Override
    public MeetUser findById(Integer idMeetUser) {
        return meetUserRepository.findById(idMeetUser).orElseThrow(() -> new EntityNotFoundException(ENTITY_NOT_FOUND));
    }

    @Override
    public Integer countAsists(Integer idMeet) {
        return meetUserRepository
                .countByMeetAndStatus(idMeet
                        , statusRepository.getOne(2)) + meetUserRepository
                .countByMeetAndStatus(idMeet
                        , statusRepository.getOne(3));
    }
}
