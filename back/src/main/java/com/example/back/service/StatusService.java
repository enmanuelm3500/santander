package com.example.back.service;

import com.example.back.entity.Status;

public interface StatusService {

    Status findById(Integer id);

}
