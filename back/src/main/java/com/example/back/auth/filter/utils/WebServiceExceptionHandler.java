package com.example.back.auth.filter.utils;

import com.example.back.handler.RestResponseEntityExceptionHandler;
import org.springframework.http.ResponseEntity;

/**
 * Esta clase de conveniencia ayuda a manejar las excepciones que puedan surgir al momento de validar un token o durante
 * la generación del mismo.
 * <p>
 * Lo ideal sería usar {@link RestResponseEntityExceptionHandler}, pero, los filters funcionan por encima de la capa de
 * controllers, por tal motivo {@link RestResponseEntityExceptionHandler} no es capaz de manejar estas excepciones y
 * transformarlas en {@link ResponseEntity}. Esta clase hace el trabajo de {@link RestResponseEntityExceptionHandler},
 * pero invocado desde los distintos filters.
 *
 * @since 0.4.0
 */
public class WebServiceExceptionHandler {

}
