package com.example.back.auth.filter.utils;

import com.example.back.exception.WebRequestException;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.WebRequest;

import java.util.Optional;

/**
 * Clase de conveniencia que obtiene los headers de un request.
 */
public class HttpHeaderHelper {

    public static HttpHeaders getHttpHeaders(WebRequest request) throws WebRequestException {
        HttpHeaders httpHeaders = new HttpHeaders();
        Optional.ofNullable(request).orElseThrow(() -> new WebRequestException("WebRequest cannot be null"))
                .getHeaderNames()
                .forEachRemaining(
                        headerName ->
                                httpHeaders.set(headerName, request.getHeader(headerName)
                                ));
        return httpHeaders;
    }
}
