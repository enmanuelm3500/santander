package com.example.back.controller;


import com.example.back.controller.mapper.CityMapper;
import com.example.back.controller.mapper.MeetMapper;
import com.example.back.dto.MeetDTO;
import com.example.back.service.CityService;
import com.example.back.service.MeetService;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController extends AbstractController{

    private CityMapper cityMapper;

    private CityService cityService;

    public CityController(CityService cityService) {
        setCityService(cityService);
        setCityMapper(Mappers.getMapper(CityMapper.class));
    }


    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<MeetDTO>> getList() {
        return createOkStatusResponseEntity(cityMapper.toDtoList(cityService.getList()));
    }

    public void setCityMapper(CityMapper cityMapper) {
        this.cityMapper = cityMapper;
    }

    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }
}
