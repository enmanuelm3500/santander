package com.example.back.controller.mapper;

import com.example.back.dto.CityDTO;
import com.example.back.entity.City;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CityMapper {

    CityDTO toDto(City entity);

    City toEntity(CityDTO dto);

    List<CityDTO> toDtoList(List<City> dto);

}
