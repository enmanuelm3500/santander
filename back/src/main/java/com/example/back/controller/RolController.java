package com.example.back.controller;

import com.example.back.dto.RolDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/rol")
public class RolController extends AbstractController {

    @GetMapping("/listByUser")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<List<RolDTO>> getList(Authentication authentication) {
        List<RolDTO> roles = authentication.getAuthorities()
                .stream()
                .map(o -> setRoles(o))
                .collect(Collectors.toList());
        return createOkStatusResponseEntity(roles);
    }

    private RolDTO setRoles(GrantedAuthority rol) {
        return new RolDTO(rol.getAuthority());
    }


}
