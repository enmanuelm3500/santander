package com.example.back.controller.mapper;

import com.example.back.dto.CityDTO;
import com.example.back.dto.MeetDTO;
import com.example.back.entity.City;
import com.example.back.entity.Meet;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MeetMapper {

    @Mappings(value = {
            @Mapping(target = "city", expression = "java(convertCityToDto(entity))")
    })
    MeetDTO toDto(Meet entity);

    @Mappings(value = {
            @Mapping(target = "city", expression = "java(convertCityToEntity(dto))")
    })
    Meet toEntity(MeetDTO dto);

    List<MeetDTO> toDtoList(List<Meet> dto);

    default CityDTO convertCityToDto(Meet meet){
        CityMapper cityMapper = Mappers.getMapper(CityMapper.class);
        return cityMapper.toDto(meet.getCity());
    }

    default City convertCityToEntity(MeetDTO meet){
        CityMapper cityMapper = Mappers.getMapper(CityMapper.class);
        return cityMapper.toEntity(meet.getCity());
    }

}
