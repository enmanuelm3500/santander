package com.example.back.controller;


import com.example.back.dto.SingleWatherDTO;
import com.example.back.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherController extends AbstractController {

    @Autowired
    private WeatherService weatherService;


    @GetMapping("/today/{idCity}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<SingleWatherDTO> getList(@PathVariable Integer idCity) {
        return createOkStatusResponseEntity(weatherService.reviewWeather(idCity));
    }
}
