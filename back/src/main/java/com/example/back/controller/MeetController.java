package com.example.back.controller;

import com.example.back.controller.mapper.MeetMapper;
import com.example.back.dto.BasicDTO;
import com.example.back.dto.MeetDTO;
import com.example.back.dto.MeetUserDTO;
import com.example.back.service.MeetService;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/meet")
public class MeetController extends AbstractController {

    private MeetMapper meetMapper;

    private MeetService meetService;

    public MeetController(MeetService meetService) {
        setMeetService(meetService);
        setMeetMapper(Mappers.getMapper(MeetMapper.class));
    }

    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<List<MeetDTO>> getList(Authentication authentication) {
        return createOkStatusResponseEntity(setMeetUserDTO(meetMapper.toDtoList(meetService.getList()), authentication));
    }

    @GetMapping("/changeStatus/{idMeetUser}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<MeetUserDTO> getList(@PathVariable Integer idMeetUser) {
        return createOkStatusResponseEntity(meetService.changeStatus(idMeetUser));
    }

    @GetMapping("/count/{idMeet}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<BasicDTO> getCountAsists(@PathVariable Integer idMeet) {
        return createOkStatusResponseEntity(meetService.countAsists(idMeet));
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MeetDTO> create(Authentication authentication, @RequestBody MeetDTO dto) {
        return createOkStatusResponseEntity(meetMapper.toDto(meetService.createMeet(meetMapper.toEntity(dto))));
    }

    private List<MeetDTO> setMeetUserDTO(List<MeetDTO> dtoList, Authentication authentication) {
        return dtoList
                .stream()
                .map(meetDTO -> this.setMeetUser(meetDTO, authentication)
                ).collect(Collectors.toList());
    }

    private MeetDTO setMeetUser(MeetDTO dto, Authentication authentication) {
        return (meetService.setMeetUser(dto, authentication));
    }

    public void setMeetMapper(MeetMapper meetMapper) {
        this.meetMapper = meetMapper;
    }

    public void setMeetService(MeetService meetService) {
        this.meetService = meetService;
    }
}
