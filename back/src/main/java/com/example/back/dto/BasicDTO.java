package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasicDTO {

    @JsonProperty("message")
    private Integer message;

    public BasicDTO() {
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }
}
