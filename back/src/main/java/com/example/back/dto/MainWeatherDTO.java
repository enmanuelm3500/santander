package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MainWeatherDTO {

    @JsonProperty("temp")
    private double temp;

    @JsonProperty("humidity")
    private String humidity;

    public MainWeatherDTO() {
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}
