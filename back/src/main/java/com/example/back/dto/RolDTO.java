package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RolDTO {

    @JsonProperty("rol")
    private String rol;

    public RolDTO() {
    }

    public RolDTO(String rol) {
        this.rol = rol;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
