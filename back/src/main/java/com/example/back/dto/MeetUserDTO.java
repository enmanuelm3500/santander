package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MeetUserDTO {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("status")
    private Integer status;

    public MeetUserDTO() {
    }

    public MeetUserDTO(Integer id, Integer status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
