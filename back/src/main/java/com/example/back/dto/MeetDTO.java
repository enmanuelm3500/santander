package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MeetDTO {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("street")
    private String street;

    @JsonProperty("houseNumber")
    private String houseNumber;

    @JsonProperty("postalCode")
    private String postalCode;

    @JsonProperty("city")
    private CityDTO city;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("date")
    private String date;

    @JsonProperty("email")
    private String email;

    @JsonProperty("topic")
    private String topic;

    @JsonProperty("idMeetUser")
    private Integer idMeetUser;

    @JsonProperty("status")
    private Integer status;



    public MeetDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getIdMeetUser() {
        return idMeetUser;
    }

    public void setIdMeetUser(Integer idMeetUser) {
        this.idMeetUser = idMeetUser;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
