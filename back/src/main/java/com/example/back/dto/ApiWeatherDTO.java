package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiWeatherDTO {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty(value = "cod")
    private String cod;

    @JsonProperty(value = "timezone")
    private String timezone;

    @JsonProperty(value = "main")
    private MainWeatherDTO main;

    public ApiWeatherDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public MainWeatherDTO getMain() {
        return main;
    }

    public void setMain(MainWeatherDTO main) {
        this.main = main;
    }
}
