package com.example.back.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleWatherDTO {

    @JsonProperty("temperatura")
    private double temperatura;

    public SingleWatherDTO() {
    }

    public SingleWatherDTO(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }
}
