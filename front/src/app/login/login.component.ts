import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserLogin} from '../core/model/user-login';
import {AuthenticationService} from '../core/services/authetication.service';
import {Router} from '@angular/router';
import {UserSession} from '../core/model/user-session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  user: UserLogin = new UserLogin();
  error: boolean;
  submit = false;

  constructor(
    private antheticationService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = this.createForm();

  }

  createForm() {
    return new FormGroup({
        username: new FormControl(null, [Validators.required]),
        password: new FormControl(null, Validators.required)
      }
    );
  }

  authentication(user: UserLogin) {
    this.antheticationService
      .authentication(user).subscribe(value => {
      if (value.status === 200) {
        let dataAuth: UserSession = {
          token: value.body.token,
          username: value.body.user
        }
        this.antheticationService.saveSession(dataAuth);
        this.router.navigate(['/home']);
      }
    });
  }

  login() {
    this.submit = true;
    if (this.form.valid) {
      Object.assign(this.user, this.form.getRawValue());
      this.authentication(this.user);
    }
  }

}

