import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RolService} from '../core/services/rol.service';
import {Rol} from '../core/model/rol';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  roles: Rol[];

  admin = false;
  user: true;

  constructor(private router: Router,
              private rolService: RolService) {
    if (!sessionStorage.getItem('login')) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.rolService.getListByUser().subscribe((value: Rol[]) => {
      this.rolService.saveRoles(value);
      this.roles = value;
    });
  }


  logout() {
    sessionStorage.removeItem('login');
    this.router.navigate(['/login']);

  }

}
