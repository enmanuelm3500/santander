import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetupsBrowserComponent } from './meetups-browser.component';

describe('MeetupsBrowserComponent', () => {
  let component: MeetupsBrowserComponent;
  let fixture: ComponentFixture<MeetupsBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetupsBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetupsBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
