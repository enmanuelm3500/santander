import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Meet} from '../../../core/model/meet';
import {MeetService} from '../../../core/services/meet.service';
import {WeatherService} from '../../../core/services/weather.service';
import {Rol} from '../../../core/model/rol';

@Component({
  selector: 'app-meetups-browser',
  templateUrl: './meetups-browser.component.html',
  styleUrls: ['./meetups-browser.component.css']
})
export class MeetupsBrowserComponent implements OnInit {

  meets: Meet[];
  meetSelected: Meet = new Meet();
  temperatura = 0;
  birras = 0;
  invitados = 0;

  roles: Rol[];
  admin = false;
  usuario = true;

  constructor(
    private meetService: MeetService,
    private route: Router,
    private weatherService: WeatherService
  ) {
  }

  ngOnInit(): void {
    this.chargeData();
    this.chargeRoles();
  }

  chargeRoles() {
    this.roles = JSON.parse(sessionStorage.getItem('roles'));
    this.roles.forEach(value => {
      if (value.rol === 'ROLE_ADMIN') {
        this.admin = true;
        this.usuario = false;
      }
    });
  }

  chargeData() {
    this.meetService.getList().subscribe((result: Meet[]) => {
      this.meets = result;
    });
  }

  createClient() {
    this.route.navigate(['/meetups/create'], {skipLocationChange: true});
  }

  reviewWeather(meet: Meet) {
    this.meetService.countAssists(meet.id).subscribe(value => {
      this.invitados = value.message;
    })

    this.weatherService.getWeather(meet.city.id).subscribe(value => {
        this.meetSelected = meet;
        this.temperatura = value.temperatura;
        this.mountBeer();
    });
  }

  mountBeer() {
    if (this.temperatura < 20) {
      this.birras = this.invitados * 0.75;
    } else if (this.temperatura > 20 && this.temperatura < 24) {
      this.birras = this.invitados * 1;
    } else {
      this.birras = this.invitados * 2;
    }
  }

  changeStatus(meet: Meet) {
    this.meetService.changeStatusForUser(meet.idMeetUser).subscribe(value => {
      if (value) {
      this.ngOnInit();
      }
    });
  }
}
