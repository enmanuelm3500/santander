import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Meet} from '../../../core/model/meet';
import {MeetService} from '../../../core/services/meet.service';
import {CityService} from '../../../core/services/city.service';
import {City} from '../../../core/model/city';
import {Router} from "@angular/router";

@Component({
  selector: 'app-meetup-create',
  templateUrl: './meetup-create.component.html',
  styleUrls: ['./meetup-create.component.css']
})
export class MeetupCreateComponent implements OnInit {


  form: FormGroup;
  meet: Meet = new Meet();
  submit = false;
  cities: City[];
  citySelected: City = new City();

  constructor(
    private meetService: MeetService,
    private cityService: CityService,
    private route: Router,

  ) {
  }

  ngOnInit(): void {
    this.form = this.createForm();
    this.cityService.getList().subscribe((result: City[]) => {
      this.cities = result;
    });
  }

  createForm() {
    return new FormGroup({
      topic: new FormControl('', [Validators.required]),
      date: new FormControl('', [Validators.required]),
      street: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      houseNumber: new FormControl('', Validators.required),
      city: new FormControl('', [Validators.required]),
      postalCode: new FormControl('', [Validators.required]),

    });

  }

  onSubmit() {
    this.submit = true;
    if (this.form.valid) {
      Object.assign(this.meet, this.form.getRawValue());
      this.citySelected.id = this.form.get('city').value;
      this.meet.city = this.citySelected;
      this.meetService.create(this.meet).subscribe(value => {
        if(value){
          this.route.navigate(['/meetups'], {skipLocationChange: true});
        }
      });
    }
  }
}
