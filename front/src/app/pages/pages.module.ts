import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {PAGES_ROUTES} from './pages.routes';
import {PagesComponent} from './pages.component';
import { MeetupsBrowserComponent } from './meetups/meetups-browser/meetups-browser.component';
import { MeetupCreateComponent } from './meetups/meetup-create/meetup-create.component';

@NgModule({
  declarations: [
    PagesComponent,
    MeetupsBrowserComponent,
    MeetupCreateComponent

  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,

  ],
  imports: [
    CommonModule,
    SharedModule,
    PAGES_ROUTES,
    FormsModule,
    ReactiveFormsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class PagesModule {
}
