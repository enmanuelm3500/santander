import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {HomeComponent} from './home/home.component';
import {MeetupsBrowserComponent} from './meetups/meetups-browser/meetups-browser.component';
import {MeetupCreateComponent} from './meetups/meetup-create/meetup-create.component';


const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'meetups', component: MeetupsBrowserComponent},
      {path: 'meetups/create', component: MeetupCreateComponent},


      {path: '', redirectTo: '/home', pathMatch: 'full'}
    ]
  },
];
export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
