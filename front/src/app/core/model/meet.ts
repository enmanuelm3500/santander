import {City} from './city';

export class Meet {
  topic: string;
  houseNumber: string;
  id: string;
  lat: string;
  lng: string;
  phone: string;
  postalCode: string;
  street: string;
  city: City;
  date: string;
  email: string;
  idMeetUser: string;

}
