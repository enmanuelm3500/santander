import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Meet} from '../model/meet';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MeetService {

  url = environment.url;


  constructor(
    private httpClient: HttpClient
  ) {
  }

  getList(): Observable<Meet[]> {
    return this.httpClient.get<Meet[]>(this.url + 'meet/list');
  }

  create(meet: Meet): Observable<Meet> {
    return this.httpClient.post<Meet>(this.url + 'meet/create', meet);
  }

  changeStatusForUser(idMeetUser): Observable<any> {
    return this.httpClient.get<any>(this.url + 'meet/changeStatus/' + idMeetUser );
  }

  countAssists(idMeet): Observable<any> {
    return this.httpClient.get<any>(this.url + 'meet/count/' + idMeet );
  }

}
