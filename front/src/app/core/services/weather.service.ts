import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Meet} from '../model/meet';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  url = environment.url;

  constructor(
    private httpClient: HttpClient
  ) { }

  getWeather(idCity: string): Observable<any> {
    return this.httpClient.get<any>(this.url + 'weather/today/' + idCity );
  }
}
