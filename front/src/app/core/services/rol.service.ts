import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Rol} from '../model/rol';


@Injectable({
  providedIn: 'root'
})
export class RolService {

  url = environment.url;


  constructor(
    private httpClient: HttpClient
  ) {
  }

  getListByUser(): Observable<Rol[]> {
    return this.httpClient.get<Rol[]>(this.url + 'rol/listByUser');
  }

  saveRoles(roles: Rol[]) {
    sessionStorage.setItem('roles', JSON.stringify(roles));
  }

}
