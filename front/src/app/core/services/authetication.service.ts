import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserLogin} from '../model/user-login';
import {UserSession} from '../model/user-session';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  url = environment.url;

  userSession: UserSession;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  authentication(data: UserLogin): Observable<any> {
    return this.httpClient.post(this.url + 'api/login', data, {observe: 'response'});
  }

  saveSession(dataAuth: UserSession) {
    sessionStorage.setItem('login', JSON.stringify(dataAuth));
  }

  cargarSession() {
    if (sessionStorage.getItem('login')) {
      this.userSession = JSON.parse(sessionStorage.getItem('login'));
    }
  }
}
