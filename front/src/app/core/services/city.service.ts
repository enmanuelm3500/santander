import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {City} from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  url = environment.url;


  constructor(
    private httpClient: HttpClient
  ) {
  }

  getList(): Observable<City[]> {
    return this.httpClient.get<City[]>(this.url + 'city/list');
  }
}
